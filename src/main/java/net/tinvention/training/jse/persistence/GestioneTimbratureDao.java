package net.tinvention.training.jse.persistence;

import java.sql.SQLException;
import java.util.List;
import net.tinvention.training.jse.dto.UtenteDto;

public interface GestioneTimbratureDao {

	public void aggiungiRigaTabellaDatabase(UtenteDto u) throws SQLException;

	public void eliminaRigaTabellaDatabase(int numMatricola) throws SQLException;

	public UtenteDto cercaUtenteTabellaDatabase(int numMatricola) throws SQLException;

	public void aggiornaTimbratureTabellaDatabase(int numMatricola) throws SQLException;

	public List<UtenteDto> restituisciListaUtentiTabellaDatabase() throws SQLException;
}
